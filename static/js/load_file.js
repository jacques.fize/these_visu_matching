function loadfile(route, node_id, containerid) {
    $("#" + containerid).loading("stop");
    $("#" + containerid).html("");
    $("#" + containerid).loading("start");
    $.post(route, {
        id: node_id
    }).done(function(response) {
        $("#" + containerid).loading("stop");
        $("#" + containerid).html(response)
    }).fail(function() {
        $("#" + containerid).loading("stop");
        $("#" + containerid).text("('Error: Could not contact server.')");
    });
}

function loadpdf(node_id, container_body, containerid_title) {
    $.post("/pdfprevhtml", {
        id: node_id
    }).done(function(response) {
        $("#" + containerid_title).loading("stop");
        $("#" + containerid_title).html(response)
        $("#" + container_body).html("<embed src=\"pdfhtml/" + node_id + "\" class='col-lg-12' style='height:100vh'>");
    }).fail(function() {
        $("#" + containerid).loading("stop");
        $("#" + containerid).text("('Error: Could not contact server.')");
    });


}
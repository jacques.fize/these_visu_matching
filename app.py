from flask import Flask, render_template, request, Markup, send_file


import mammoth#docx
import textract # doc
import json,os, subprocess
import pandas as pd

data_file_bvlac = json.load(open("assoc_file_bvlac.json"))
app = Flask(__name__)

@app.route('/')
def hello_world():
    return render_template("graph.html", filename="graph_files/BVLAC_MCS_str_object.npy.bz2.json")

def filename_header(filename):
    format2font_awesome = {
        "docx":"file-word",
        "doc":"file-word",
        "xls":"file-excel",
        "xlsx":"file-excel",
        "pdf":"file-pdf",
        "txt":"file",
        "html":"file-code"
    }
    ext = filename.split(".")[-1]
    if not ext in format2font_awesome:ext = "txt"
    return "fa fa-{0}".format(format2font_awesome[ext])

def render_preview(filename,html):
    return """
        <div id="preview-header">
            <span class="h4"><i class='{icon}'></i></span>  <span class="p">{filename}</span>
            <br>
            <div class="container" style="background:#ddd">
                <span class="h6">Path</i></span>  <span style="font-size:9px;" class="p">{path}</span>
            </div>
        </div>
        <hr>
        <div id="preview-body">
        {html}
        </div>
        """.format(icon=filename_header(filename),filename=filename.split("/")[-1].replace("-"," "),html=html,path=filename)

@app.route('/pdfprevhtml',methods=["POST"])
def render_preview_pdf():
    filename = data_file_bvlac[str(request.form["id"])]
    return render_preview(filename,"")

@app.route('/docxhtml',methods=["POST"])
def get_docx_html():
    """
    Return HTML version of a docx
    
    Returns
    -------
    str
        HTML
    """
    filename = data_file_bvlac[str(request.form["id"])]
    with open(filename, "rb") as docx_file:
        result = mammoth.convert_to_html(docx_file)
        html = result.value
        return render_preview(filename,html)
        

@app.route('/dochtml',methods=["POST"])
def get_doc_html():
    """
    Return HTML version of a .doc file
    
    Returns
    -------
    str
        HTML
    """
    if not os.path.exists("temp"):
        os.makedirs("temp")
    filename = data_file_bvlac[str(request.form["id"])]
    new_filename = "temp/"+ filename.split("/")[-1].replace(".doc",".docx")
    command_run = 0
    if not os.path.exists(new_filename):
        command_run = subprocess.call(['soffice', '--headless', '--convert-to', 'docx',
        "--outdir","temp", filename])
    if  command_run !=0:
        return "ERROR while parsing the doc file"
    
    with open(new_filename, "rb") as docx_file:
        result = mammoth.convert_to_html(docx_file)
        html = result.value
        return render_preview(filename,html)

@app.route('/xlsxhtml',methods=["POST"])
@app.route('/xlshtml',methods=["POST"])
def get_xlsx_html():
    """
    Return HTML version of a .xls and .xlsx file
    
    Returns
    -------
    str
        HTML
    """
    filename = data_file_bvlac[str(request.form["id"])]
    df = pd.read_excel(filename).head(200)
    return render_preview(filename,df.to_html())

@app.route('/txthtml',methods=["POST"])
def get_txt_html():
    """
    Return HTML version of a .txt file
    
    Returns
    -------
    str
        HTML
    """
    filename = data_file_bvlac[str(request.form["id"])]
    txt = open(filename).read()
    return Markup("<h4>"+filename+"</h4><br>"+"<p>{0}</p>".format(txt).replace("\n",""))


@app.route('/htmlhtml',methods=["POST"])
def get_html_html_with_id():
    """
    Return HTML version of an .html file
    
    Returns
    -------
    str
        HTML
    """
    filename = data_file_bvlac[str(request.form["id"])]
    print(filename)
    return render_preview(filename,"<p>{0}</p>".format(open(filename, 'r').read())) 

@app.route('/pdfhtml/<int:id_doc>')
def get_pdf_html_with_id(id_doc):
    """
    Return the pdf
    
    Returns
    -------
    str
        HTML
    """
    filename = data_file_bvlac[str(id_doc)]
    static_file = open(filename, 'rb')
    return send_file(static_file, attachment_filename='file.pdf')

if __name__ == '__main__':
    app.run(debug=True)

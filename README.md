# Visualization Tool for heterogeneous textual data matching

This tool is a web-application that allows to visualize and browse document matching based on a specific representation and similarity measure.

# Requirements

* Python 3
* Linux and Mac OS X
* OpenOffice (for *.doc* file preview)


# Launch the program
To run the app :

```bash
python3 app.py
```
Then, open your browser at the following address : http://127.0.0.1:5000/